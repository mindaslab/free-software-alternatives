A list of software that can help you to compute in freedom.

# Accounting

Tally - [GNU Khata](http://gnukhata.in/)

# Blogging

Medium - [Wordpress](http://wordpress.org)

# Email

Outlook - [Thunder Bird](https://www.mozilla.org/en-US/thunderbird/)

# Graphics

Corel Draw - [Inkscape](http://inkscape.org)

Adobe Photoshop - Gimp

# Office Suite

Microsoft Office - [Libre Office](https://www.libreoffice.org/)

# Operating System

Windows - [Ubuntu](https://ubuntu.com)

# Scientific Computing

Matlab - [Gnu Octave](https://www.gnu.org/software/octave/)

# Social Networking

Facebook - [Diaspora](https://diasporafoundation.org/)

Twitter - [Mastadon](https://mastodon.social/about)

# Version Control

Github - [GitLab](https://gitlab.com/)

# Video

? - Vlc

# Video creation & Editing

? - Blender
? - PiTiVi, OpenShot
